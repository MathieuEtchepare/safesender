Components.utils.import("resource:///modules/mailServices.js"); 

window.addEventListener('compose-window-init', function(event) {
  // console.log('event', event)
  const composeWindow = event.currentTarget
  var SendMessageOriginal = composeWindow.SendMessage
  composeWindow.SendMessage = function() {
    bComposeSealAndSend(SendMessageOriginal) //WHAT EVER THE CUSTOM METHOD NEEDS TO BE CALLED
  }
}, true)

function bComposeSealAndSend(SendMessageOriginal) {
  try {
    // var compFields = Components.classes["@mozilla.org/messengercompose/composefields;1"].createInstance(Components.interfaces.nsIMsgCompFields);
    var myContent = document.getElementById("content-frame").contentDocument.lastChild.lastChild;
    var strSUBJECT = document.getElementById("msgSubject").value;
    var strBODY = myContent.innerText
    var strFROM = document.getElementById("msgIdentity").label;
    httpRequest1(SendMessageOriginal, strSUBJECT, strBODY, strFROM)
  } catch (err) {
    alert(err)
  }
}

function httpRequest1 (SendMessageOriginal, subject, body, from) {
  const xhttp = new XMLHttpRequest()
  xhttp.onreadystatechange = function () {
    if (this.readyState === 4 && this.status === 200) {
      const response = JSON.parse(xhttp.responseText)
      console.log(response)
      if (response.return === false) {
        codeLoop(false, from, send, SendMessageOriginal)
      }
    }
  }
  xhttp.open('POST', 'http://localhost:5000/email', true)
  xhttp.send(JSON.stringify({ subject: subject, body: body, mail: from }))
}

function codeLoop (verified, from, callback, SendMessageOriginal) {
  const code = prompt('Your mail is suspicious, a text message has been sent to your phone number with a code', 'code')
  if (code === null) {  //  Si on clique sur annuler, on quitte la boucle
    return false
  }
  const xhttp2 = new XMLHttpRequest()
  xhttp2.onreadystatechange = function () {
    if (this.readyState === 4 && this.status === 200) {
      const answer2 = JSON.parse(xhttp2.responseText)
      callback(answer2.return, from, SendMessageOriginal)
    }
  }
  xhttp2.open('POST', 'http://localhost:5000/checkCode', true)
  xhttp2.send(JSON.stringify({ code: code, mail: from }))
}

function send(answer, from, SendMessageOriginal) {
  if (answer) {
    SendMessageOriginal.apply(this, arguments)
  } else {
    alert('code erroné!')
    codeLoop(false, from, send, SendMessageOriginal)
  }
}
