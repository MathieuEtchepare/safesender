from flask import Flask, session, request, jsonify
from flask_restplus import Api, Resource, fields
from flask_cors import CORS
import json
import ssl
import settings
import utils
import sys
import data
import re 

flask_app = Flask(__name__)
CORS(flask_app)
api = Api(flask_app)

resource_fields = api.model('Email', {
    'body': fields.String,
    'subject': fields.String,
    'mail': fields.String
})

@api.route("/email")
class MainClass(Resource):
    @api.expect(resource_fields)
    def post(self):
        try:
            received_data = request.get_json(force = True)
            payload = dict()
            print(received_data)
            phoneNumber = data.get_phone(received_data['mail'])
            if phoneNumber != False:
                if utils.test_mail(received_data['subject'] + " " + received_data['body']):
                    payload['return'] = True
                else:
                    utils.send_code(received_data['mail'])
                    payload['return'] = False
                return jsonify(payload)
            else:
                return {'errorCode': 1}
        except Exception as e:
            print(e)

@api.route("/checkCode")
class Code(Resource):
    def post(self):
        try:
            data = request.get_json(force = True)
            payload = dict()
            if utils.compare_codes(data['code'], data['mail']):
                payload['return'] = True
            else:
                payload['return'] = False
            return jsonify(payload)
        except Exception as e:
            print(e)

@api.route('/login')
@api.expect(api.model('Login', {
    'mail': fields.String,
    'password': fields.String
}))
class Login(Resource):
    def get(self):
        received_data = request.args
        if received_data.get('token') and data.is_token_valid(received_data.get('token')):
            account = data.get_account(received_data.get('token'))
            return account
        else:
            return False
    def post(self):
        received_data = request.get_json()
        token = data.auth_account(received_data['mail'], received_data['password'])
        payload = {}
        if token is not None and token != False:
            payload['token'] = token
        elif token == False:
            payload['error'] = 'Invalid email address or password'
        else:
            payload['error'] = 'User already connected'
        return payload

@api.route('/logout')
@api.expect(api.model('Logout', {
    'token': fields.String
}))
class Logout(Resource):
    def post(self):
        received_data = request.get_json()
        if 'token' in received_data and data.is_token_valid(received_data['token']):
            token = received_data['token']
            data.close_session(token)
            return True
        return False

@api.route('/register')
@api.expect(api.model('Register', {
    'email': fields.String,
    'password': fields.String,
    'phone-number': fields.String
}))
class Register(Resource):
    def post(self):
        received_data = request.get_json()
        if 'mail' in received_data and 'password' in received_data and 'phone-number' in received_data:
            success = data.add_account(received_data['mail'], received_data['password'], received_data['phone-number'])
            if success:
                return True
            else: return {'error': 'This email address is already in use'}
        else:
            return False
    
    def update(self):
        received_data = request.get_json()
        if 'token' in received_data and data.is_token_valid(received_data['token']):
            # modify account
            return True
        else:
            return False


@api.route('/health')
class Health(Resource):
    def get(self):
        payload = dict()
        payload['online'] = True
        return jsonify(payload)
    
# context = ssl.SSLContext(ssl.PROTOCOL_TLSv1_2)
# context.load_cert_chain(settings.OPENSSL_CERT, settings.OPENSSL_KEY)

# flask_app.run(ssl_context=context, debug=True)
if len( sys.argv ) >= 1:
    flask_app.run(host=sys.argv[1], debug=True)
else: 
    flask_app.run(debug=True)