from pymongo import MongoClient
import bcrypt
import random

client = MongoClient("mongodb+srv://mathieu:FmBx5VNwbQeZ5Th45@cluster0-phqqw.mongodb.net/test?retryWrites=true&w=majority")
db = client.safesender
sessions = dict()


def hash_password(password):
    salt = bcrypt.gensalt()
    return bcrypt.hashpw(password.encode('utf8'), salt)

def check_password(hashed, password):
    return bcrypt.checkpw(password.encode('utf8'), hashed)

def add_account(mail, password, phone):
    res = db.accounts.find_one({
        "email": mail,
    })
    if res is None:
        newAccount = {
            "email": mail,
            "password": hash_password(password),
            "phone": phone
        }
        account_id = db.accounts.insert_one(newAccount).inserted_id
        print(account_id)
        return True
    else:
        return False

def is_mail_in_use(mail):
    res = db.accounts.find_one({
        "email": mail
    })
    if res is not None:
        return True
    else:
        return False

def get_account(token):
    res = db.accounts.find_one({
        "email": sessions[token],
    })
    return {'email': res['email'], 'phone-number': res['phone']}

def find_session_token_by_mail(mail_to_find):
    for token, mail in sessions.items():
        if mail == mail_to_find:
            return True
    return False

def is_token_valid(token):
    return token in sessions

def auth_account(mail, password):
    res = db.accounts.find_one({
        "email": mail,
    })
    if res is not None:
        if check_password(res['password'], password):
            if find_session_token_by_mail(mail) == False:
                token = None
                # tant que le token n'est pas defini ou s'il en genere un deja utilise
                while token is None:
                    token = hash_password(str(random.randint(1, 999999)))
                    if token in sessions:
                        token = None
                # on initialise la session
                token = str(token)
                sessions[token] = mail
                return token
            else:
                return None
        else:
            return False
    else: return False

def close_session(token):
    if token in sessions:
        del sessions[token]
    else:
        print('Entry not found')

def get_phone(mail):
    res = db.accounts.find_one({
        "email": mail,
    })
    if res is not None:
        return res['phone']
    else: return False