Résumé
======

SafeSender est une API reliée à un modèle de machine learning ayant une action
anti-spam et anti-phishing.

Les mails envoyés par des particuliers passent par une analyse avant l'envoi
afin de n’envoyer que du contenu considéré comme sûr pour les destinataires. 

Si le modèle de machine learning détecte que le mail envoyé est du spam, un
système de double authentification est utilisé. Ainsi, l’utilisateur, qui a
préalablement renseigné un numéro de téléphone reçoit une notification par sms
pour lui indiquer qu’un mail suspecté comme étant du spam vient d’être envoyé de
sa boîte mail. La personne pourra ainsi agir en conséquence si ce n’est pas elle
qui a envoyé ce mail, puisqu’elle aura la preuve et la certitude que sa boîte
mail a été infectée et est utilisée à son insu pour de la propagation de
contenus infectés et potentiellement frauduleux.

Le but est d’empêcher d’éventuels automates d’envoyer des spams par le biais de
l’adresse mail de l’utilisateur, ainsi que la propagation de contenu infecté. 

L'autre objectif est de protéger l’utilisateur, responsable des mails qu’il
envoie, et lui éviter d’être placé sur liste noire par les hébergeurs mail
classiques.

Mots-clés
=========

Machine Learning, Cybersécurité, anti-spam, anti-phishing, détection à l’envoi

Contexte
========

Besoins et utilisateurs
-----------------------

Le trafic réseau, chaque jour, est saturé de courriels, et notamment de
pourriels. En effet des études estiment qu’en 2018, environ 14,5 milliards de
courriels frauduleux étaient envoyés chaque jour. Cela correspond à 45% du total
de mails envoyés selon ces mêmes études. Ces quantités massives sont parfois le
fait d'un seul attaquant, mais jamais d'une seule machine.

Un réseau de machines zombie (aussi connu sous son terme anglais botnet) est
constitué d'un grand nombre de machines connectées à une personne, qui peut en
prendre le contrôle.

On peut donc imaginer qu'une personne a un jour récupéré un virus, qui connecte
son ordinateur au réseau de machines zombie [fig 1]. Cette infection ne sera pas
remarquée avant l'activation du réseau, qui peut prendre quelques mois selon que
l'attaquant soit pressé ou non. Lorsque le réseau est activé par l'attaquant,
chaque machine va effectuer une action, telle que se connecter à une cible afin
de causer un déni de service, ou encore envoyer un nombre important de courriels
indésirables [fig 2].

![](media/6ac42e44c0d33663e4c176d67fb8fbdb.png)

![](media/291575892dae752f0343d596105efa61.png)

Il peut donc être intéressant pour un utilisateur exposé aux risques du net de
savoir que ce scénario peut lui arriver, mais qu'à ce moment, un outil lui
assurera une certaine protection. 

Comment intégrer l’outil
------------------------

L'envoi d'un courriel, d’un point de vue réseau, passe par plusieurs étapes, où
l’interception et l’analyse peuvent avoir lieu :

-   Sur le poste utilisateur, lorsque l’envoi est demandé

-   Entre le poste client et le serveur SMTP

-   Sur le serveur SMTP

-   Envoi vers le destinataire

Une fois à ce stade de l’envoi, il n’est plus possible d’agir sur l’envoi du
mail. De plus, étant donné qu’à part certaines entreprises, très peu de
personnes ont une véritable maîtrise de leur serveur SMTP. Il convient donc de
se questionner sur le fait de placer l’outil pour agir sur le poste utilisateur,
ou sur le réseau.

### Placer en réseau

Les avantages : Juste un équipement à pleinement paramétrer, puis une
configuration rapide des machines. Bon pour l’entreprise.

Les inconvénients : Les particuliers ne connaissent pas forcément leur infra, et
il faut effectuer le déchiffrement SSL des courriels envoyés via SMTPS.

### Placer sur le poste

Les avantages : Plus pratique pour un poste unique, pas de préoccupations du
chiffrement SSL.

Les inconvénients : Besoin de pouvoir accéder et interagir avec le client
d’envoi.

Etant donné le choix de cibler les particuliers pour diffuser SafeSender, il a
été choisi de mettre en place un module pour client de messagerie, tel que
Thunderbird ou Office.

Respecter le cadre légal
------------------------

Le respect des réglementations de protections des données personnelles (RGPD)
est essentiel, et c’est pour cela que les courriels transmis au serveur en vue
d’être analysés ne sont pas stockés.

Positionnement sur le marché
----------------------------

De nombreux anti-spam existent déjà sur le marché, la plupart sont développés
pour des entreprises en filtrant les courriels reçus par les utilisateurs finaux
de l'entreprise.

Aucune solution n'existe pour filtrer le contenu du mail lors de l'envoi, car la
stratégie principale est axée sur la réception :

-   Gmail utilise une solution de machine learning qui utilise notamment
    TensorFlow et qui est intégré aux boîtes mails. Cette solution vise à
    détecter des mails à la réception dans la messagerie utilisateur et ne vise
    pas la détection à l’envoi.

-   Outlook utilise aussi en ajout à son antispam une solution de machine
    learning : Advanced Threat Protection (ATP) afin de détecter comme la
    version de Gmail la réception des emails et non leur envoi.

-   Spamassassin est une solution d’antispam libre développé par la Apache
    Software Foundation, il est disponible sur tous les systèmes d’exploitation
    et s’installe principalement sur des serveurs de gestions de mail plus que
    sur des postes individuels. Il détecte les spams via des tests qui
    attribuent un score.

-   Spamkiller est la solution d’antispam possédé par McAfee. Elle s’intègre
    avec leur antivirus et permet via des tests heuristiques de détecter les
    spams. Encore une fois il s’agit ici d’une solution de détection à la
    réception et non à l’envoi.

-   SpamTitan est une solution pour entreprise qui propose plusieurs produits
    sur la sécurisation d’office365 ou sur son propre serveur mail. Leur
    solution se base sur des tests heuristiques et propose des solutions en plus
    tels que le sandboxing pour tester les pièces jointes ou même des listes de
    quarantaines distinctes.

-   MailInBlack une entreprise française spécialisée dans l'édition de solutions
    anti-spam et antivirus propose un outil d'ajout d'utilisateurs dans des
    listes blanches et propose un ajout automatique des destinataires des mails
    dans les listes blanches. Il propose aussi un scan antiviral mais aucunement
    une détection de mail de type hameçonnage ou de pourriel.

Solution proposée et nature de Proof of Concept
===============================================

Vision globale et utilisation du service
----------------------------------------

Le produit prévu par le projet est un anti-spam/anti-phishing au service du
particulier. Il s’intègre dans une dynamique de permettre à l’utilisateur de
s’informer que son ordinateur personnel est peut-être compromis. Il y a aussi
une dimension de venir soulager les envois de mails massifs. Permettre de
diminuer les mails envoyés contribue à diminuer l’énergie que dépense Internet.

![](media/a95673d3ac9b0fac52c50ebfbaf59dc4.png)

Ici on peut observer le mécanisme de notre application qui lorsqu’un mail est
envoyé par la messagerie du client l’envoie au serveur. Le serveur va analyser
le mail avec notre algorithme et va déterminer si le mail qui va être envoyé est
légitime. Si celui-ci est légitime, alors le mail est envoyé et rien de plus
n’est fait. Si le mail est frauduleux l’utilisateur est prévenu.

Enjeux de sécurité et environnemental
-------------------------------------

Grâce à notre produit, nous souhaitons permettre à tous d’accéder à la sécurité.
Le vecteur premier étant l’infection par courriel, surtout dans un contexte de
particulier où aucun service exposé ne tourne sur internet, nous voulons
permettre à tous de se rendre compte d’une compromission. Notre produit
s’inscrit donc dans un but double : aider le particulier à se rendre compte
qu’il a été piraté et éviter que celui-ci se trouve être la source d’une attaque
de mails plus grande.

D’un point de vue environnemental, les mails sont une des plus grandes dépenses
en gramme équivalent carbone pour Internet. Il est donc pertinent pour nous de
penser à éviter que des mails de spam ou de phishing soient envoyés sur le
réseau et donc stockés sur des serveurs. 

Architectures fonctionnelles et techniques
------------------------------------------

![](media/2f7442c9a48a0ec92e07b61c60f0e333.png)

Nous avons imaginé SafeSender comme un alliage de quatre modules.

Premièrement, on retrouve le module Thunderbird, qui va s’occuper de gérer
l’interception du mail et de savoir s’il faut le faire suivre ou non.

Ensuite, nous retrouvons le serveur, qui permet de répondre aux requêtes de
l’extension et du site, de gérer les connexions et la double authentification en
cas de mail douteux.

Le site va nous permettre de pouvoir authentifier les utilisateurs. Ils y
créeront un compte en enregistrant leur adresse mail et le numéro de téléphone
sur lequel ils veulent être prévenus. Toutes ces informations seront stockées
dans la base de données, ce qui permettra de retrouver l’utilisateur via son
adresse mail, sans avoir à stocker localement les informations de l’utilisateur
dans Thunderbird, ce qui est à la fois fastidieux et potentiellement risqué pour
notre cas d’utilisation.

Critères de validation du concept
---------------------------------

Ainsi notre produit nous permet de détecter des emails à caractère de spam ou
d’hameçonnage. Grâce à un plugin sur le logiciel de messagerie Thunderbird il
nous est possible d’intercepter un courriel avant que celui-ci soit envoyé. A
chaque envoi le plugin le transmet à notre serveur, dans le PoC en réseau local,
afin que celui-ci soit analysé. Une api récupère le message qui est ensuite
traité par notre algorithme de machine learning qui détermine l’authenticité du
mail. Une fois cela fait deux cas se présentent : le mail est considéré comme
légitime et dans ce cas-là le mail est envoyé par la boîte utilisateur. Le
deuxième cas est si le mail est considéré comme frauduleux, dans ce cas
l’utilisateur est notifié par sms qu’un courriel suspecté comme frauduleux a été
envoyé par sa messagerie. Ceci représente le cas d’utilisation et donc ce sur
quoi nous nous sommes concentrés dans le rendu du prototype. Les critères de
validation du Proof of Concept (PoC) sont donc :

-   la capacité à intercepter un mail et pouvoir récupérer son contenu

-   faire passer ce contenu du mail dans notre modèle de machine learning afin
    de déterminer si c’est un mail estimé comme étant un spam

-   dans le cas où le mail est détecté comme un spam, envoyer un sms à
    l’utilisateur pour lui signifier qu’un email estimé comme frauduleux vient
    d’être envoyé avec sa boîte mail.

Réalisation et état d’avancement
================================

Choix techniques
----------------

Au niveau du machine learning, nous avons aussi dû faire différents choix
techniques. Il a d’abord fallu choisir un dataset comprenant des données
facilement utilisables pour l’entraînement de notre modèle. Ensuite, il a fallu
déterminer quel algorithme de machine learning utiliser. Après avoir regardé
certaines études déjà existantes utilisant le machine learning pour la détection
de spam, nous avons fait le choix de tester différents algorithmes et de les
comparer. Ainsi, nous avons entraîné et testé les algorithmes suivants sur notre
jeu de données test :

-   BERT fine Tuning classification

-   Multinomial Naive Bayes

-   Logistic Regression

-   Complement Naive Bayes

Après avoir testé et comparé ces différents algorithmes, notre choix s’est porté
sur le BERT fine Tuning, qui nous donnait les meilleurs résultats lors des
tests. C’est donc cet algorithme qui a été utilisé pour ce projet. La partie
machine learning du projet a été codée en langage Python.

Au niveau de l’API, nous avons choisi d’utiliser le Framework Flask qui permet
de faire des API REST en python, cela nous permet de communiquer facilement avec
le modèle sans avoir à changer de langage.

Nous utilisons une base de données MongoDB qui permet une manipulation simple,
et optimisée pour le peu de données que nous stockons (email, mot de passe et
numéro de téléphone).

Pour le site Internet, nous avons utilisé Vue.js, un framework Javascript
permettant de réaliser un site rapidement et facilement avec un design
satisfaisant.

Fonctionnalités réalisées
-------------------------

Afin d’accueillir nos services nous avons décidé de les faire tourner sur un
système Unix : Debian 10, la machine virtuelle utilisée pour le PoC a été
durcie. La machine a donc un disque chiffré, des mots de passe utilisateur et
des mots de passe administrateur respectant les recommandations de l’ANSSI.

Au niveau du machine learning, le modèle de détection de spam a été créé, est
fonctionnel et réutilisable pour déterminer si les mails envoyés sont des
courriers frauduleux ou non. Lorsque le contenu d’un mail (objet et contenu du
mail) passe dans notre modèle, celui-ci est ainsi capable de déterminer si ce
mail est considéré comme un spam ou mail légitime. Cette réponse (spam ou non)
est renvoyée vers le serveur pour d’éventuelles futures actions comme l’envoi
d’une alerte sms à l’utilisateur.

Au niveau du machine learning, le modèle de détection de spam a été créé, est
fonctionnel et réutilisable pour déterminer si les mails envoyés sont des
courriers frauduleux ou non. Lorsque le contenu d’un mail (objet et contenu du
mail) passe dans notre modèle, celui-ci est ainsi capable de déterminer si ce
mail est considéré comme un spam ou mail légitime. Cette réponse (spam ou non)
est renvoyée vers le serveur pour d’éventuelles futures actions comme l’envoi
d’une alerte sms à l’utilisateur.

Le site permet de s’authentifier et de créer un compte, pour pouvoir modifier
ses informations personnelles.

Le serveur dispose de routes pour la connexion, la création et la modification
de compte, pour la validation de courriel, pour récupérer le code envoyé, ainsi
qu’une route de ping pour savoir s’il est en ligne. Si un mail est détecté comme
frauduleux, on envoie un SMS à l’utilisateur en cherchant l’adresse mail
transmise, dans la base de données.

Le certificat pour le chiffrement est prêt mais il n’est pas encore signé,
Thunderbird nous empêche donc de l’utiliser. Pour l’instant, sur ce Proof of
Concept, il n’est pas dramatique d’envoyer des messages non chiffrés, puisque ce
sont des mails de test. Pour la production, nous le signerons afin de pouvoir
utiliser le protocole HTTPS pour notre API.

L’extension détecte l’envoi d’un mail, l’interrompt en contactant le serveur, si
le serveur lui répond que le mail est suspicieux, une fenêtre modale s’ouvre
proposant à l’utilisateur de rentrer le code qu’il a reçu par message. Il reste
bloqué dans une boucle infinie tant qu’il n’a pas le bon code (valable pour 3
essais) ou qu’il n’a pas annulé l’envoi.

Benchmarking vis-à-vis des critères de validation 
--------------------------------------------------

| Fonctionnalité                                 | Réalisation | Précisions                                                                                                                                                                                                     |
|------------------------------------------------|-------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Modèle de machine learning efficace            | 5/5         | Le modèle est capable d’estimer si un mail est du spam ou non, cela à partir de son contenu (objet et texte)                                                                                                   |
| Implémentation dans un module pour client mail | 4/5         | Thunderbird entreprend en ce moment une migration de la classe WebExtension à MailExtension, ce qui limite les fonctionnalités disponibles. L’extension a donc été développée pour les versions plus anciennes |
| Hébergement sur une machine durcie             | 3/5         | Perte du mot de passe disque de ladite machine, qui a nécessité une nouvelle machine en dernière minute                                                                                                        |

Conclusion et perspectives
==========================

Dans cette étude, nous avons démontré que le travail d’un antispam au service du
particulier est un outil réalisable. Nous nous sommes concentrés sur un besoin
aujourd’hui nécessaire et aux travers de nos compétences transverses nous avons
pu découvrir de nouvelles connaissances.

L’idée de proposer l’utilisation de SafeSender via un module de client mail
comme Thunderbird ou Office permet de proposer une utilisation facile et rapide
de l’outil, à titre gratuit pour commencer; l’hébergement sur serveur étant
relativement faible, il serait possible d’investir un montant relativement
faible pour permettre une année de disponibilité, et acquérir une base
d’utilisateurs mais aussi de retours sur la performance du modèle, qui pourrait
être entraîné avec les courriels qu’il analyse. Il nous faut nous assurer que le
partage et l’installation d’un tel module puisse se faire facilement, car si
Thunderbird propose un partage facile des add-ons, Microsoft et son store
AppSource sont relativement peu mis en avant auprès des utilisateurs.

Au terme d’une année d’expérience, si le produit s’avérait performant, nous
aurions la possibilité de le présenter aux différentes entreprises présentes sur
le marché pour l’analyse de courriels à la réception. Au sein de l’équipe, nous
n’avons pas spécialement la fibre entrepreneuriale, mais estimons que le produit
pourrait être commercialisé par un éditeur déjà présent sur le marché. Notre
perspective est donc la vente.

Références
==========

[1] Vade Secure Launches the First Native, AI-Based Email Security Add-On for
Office 365, Ed Hadley, 2018: 

https://www.vadesecure.com/en/vade-secure-launches-the-first-native-ai-based-email-security-add-on-for-office-365/ 

[2] Office 365 helps secure Microsoft from modern phishing campaigns, Microsoft,
2019:

<https://www.microsoft.com/en-us/itshowcase/office-365-helps-secure-microsoft-from-modern-phishing-campaigns> 

[3] Office 365 ATP Anti-Phishing Features, DuoCircle, 2019:

<https://www.duocircle.com/content/office-365-phishing-protection/office-365-atp-anti-phishing>

[4] Kaspersky Security for Microsoft Office 365, Kapersky, 2019:

<https://www.kaspersky.fr/enterprise-security/microsoft-office-365> 

[5] MailinBlack, 2020:

<https://www.mailinblack.com/solution/> 

[6] 15 Outrageous Email Spam Statistics that Still Ring True in 2018, Emily
Bauer, 2018:

<https://www.propellercrm.com/blog/email-spam-statistics>

[7] Statistiques sur les spams et le phishing, les virus et ransomwares et les
publicités, Stéphane Manhes, 2019: 

<https://www.altospam.com/actualite/2019/05/statistiques-sur-les-spams-et-le-phishing-les-virus-et-ransomwares-et-les-publicites/>

[8] Symantec Internet Security Threat Report, Symantec, 2018, p. 66-73:

<https://www.phishingbox.com/assets/files/images/Symantec-Internet-Security-Threat-Report-2018.pdf> 

 

Dataset utilisé pour le training du machine learning : 

[9] Spam Mail Dataset, Venkatesh Garnepudi, 2019 :

<https://www.kaggle.com/venky73/spam-mails-dataset?fbclid=IwAR3Ae1EKLx85MLFOm-C3Nyb1MfLKJ5bdwyFQuMUteQbHda6hFqlgPeBy0qE>
